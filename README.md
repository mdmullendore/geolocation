README.md
=========================


Geolocation App

- Application already complied and ready to run as static doc. 
  nav to the directory and open the index.html into a browser (chrome)

- Tech Stack
  - Javascript
  - AJAX
  - JSON
  - Jquery
  - Angular.js
  - Bootstrap
  - HTML5
  - CSS3
