angular.module('app', ['ngMessages']);
angular.module('app').directive('urlval', function () {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function (scope, element, attr, ctrl) {
			function customValidator(ngModelValue) {
				if (/[http]/.test(ngModelValue)) {
					ctrl.$setValidity('patternValidator', false);
				} else {
					ctrl.$setValidity('patternValidator', true);
				}
				return ngModelValue;
			}
			ctrl.$parsers.push(customValidator);
		}
	};
});