function updateLocationDetails(data){
	$("#location_query").html(data.query);
	$("#location_country").html(data.country);
	$("#location_regionName").html(data.regionName);
	$("#location_city").html(data.city);
	$("#location_timezone").html(data.timezone);
	$("#location_lat").html(data.lat);
	$("#location_lon").html(data.lon);

	$("table").removeClass("empty");
	$(".help").click(function(e){
		var fieldName = $(e.currentTarget).closest('tr').find('.field_name').text();
		document.getElementById('myModalLabel').innerHTML = fieldName;
		document.getElementById('alert-body').innerHTML = "This is your " + fieldName + " from ISP " + data.isp + " at:" + "</br>" + now;
	});

	if($('#website-value').val()){

		var value = $('#website-value').val();
		var location = 'http://freegeoip.net/json/';
		var url = location + value;
		
		$.when(
			$.getJSON("http://ip-api.com/json/", {
			 	format: "json"
			}),
			$.getJSON(url, {
				format: "json"
		})).then(function (res1, res2) {

			var map;
			
			var lat = res1[0].lat;
			var lon = res1[0].lon;
			var latitude = res2[0].latitude;
			var longitude = res2[0].longitude;

			var locations = [
				['city 1', lat, lon, 2],
				['City 2', latitude, longitude, 1]
			];
			var is_internetExplorer11= navigator.userAgent.toLowerCase().indexOf('trident') > -1;
			var marker_url = ( is_internetExplorer11 ) ? 'img/cd-icon-location.png' : 'img/cd-icon-location.svg';

			var map = new google.maps.Map(document.getElementById('map-canvas'), {
				zoom: 10,
				center: new google.maps.LatLng(-39.92, 151.25),
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				scrollwheel: false,
				disableDefaultUI: true
			});

			var infowindow = new google.maps.InfoWindow();

			var marker, i;
			var markers = new Array();

			for (i = 0; i < locations.length; i++) {  
				marker = new google.maps.Marker({
					position: new google.maps.LatLng(locations[i][1], locations[i][2]),
					map: map,
					icon: marker_url,
				});

				markers.push(marker);

				google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infowindow.setContent(locations[i][0]);
					infowindow.open(map, marker);
				}
				})(marker, i));
			}

			function AutoCenter() {
				//  Create a new viewpoint bound
				var bounds = new google.maps.LatLngBounds();
				//  Go through each...
				$.each(markers, function (index, marker) {
				bounds.extend(marker.position);
				});
				//  Fit these bounds to the map
				map.fitBounds(bounds);
			}
			AutoCenter()
			$('[data-show="valid-url"]').hide();
			$('[data-show="add-url"]').hide();
		}, function() {
			// One of the sources is not available
			$('[data-show="add-url"]').show();
		});
	}
	else{
		var now = new Date();

		var map;
		var latitude = data.lat;
		var longitude = data.lon;
		var myLatlng = new google.maps.LatLng(latitude,longitude);
		var is_internetExplorer11= navigator.userAgent.toLowerCase().indexOf('trident') > -1;
		var marker_url = ( is_internetExplorer11 ) ? 'img/cd-icon-location.png' : 'img/cd-icon-location.svg';

		var mapOptions = {
			zoom: 10,
			center: myLatlng,
			scrollwheel: false,
			disableDefaultUI: true
		}
		var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			icon: marker_url
		});
	}
}
google.maps.event.addDomListener(window, 'load', updateLocationDetails);

function getMyLocation() {
	return $.ajax({
		type : 'GET',
		url : 'http://ip-api.com/json/',
		success : function(response){
			updateLocationDetails(response);
		}
	});
}

// call from My Location
function getWebSiteLocation() {
	
	var value = $('#website-value').val();
	var location = 'http://freegeoip.net/json/';
	var url = location + value;
	
	if(url == 'http://freegeoip.net/json/' ){
		$('[data-show="add-url"]').show();
	}else{
		$.when(
			$.getJSON(url, {
				format: "json"
		})).then(function (res) {

			var map;
			
			var latitude = res.latitude;
			var longitude = res.longitude;
			var myLatlng = new google.maps.LatLng(latitude,longitude);
			var is_internetExplorer11= navigator.userAgent.toLowerCase().indexOf('trident') > -1;
			var marker_url = ( is_internetExplorer11 ) ? 'img/cd-icon-location.png' : 'img/cd-icon-location.svg';

			var mapOptions = {
				zoom: 10,
				center: myLatlng,
				scrollwheel: false,
				disableDefaultUI: true
			}
			var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

			var marker = new google.maps.Marker({
				position: myLatlng,
				map: map,
				icon: marker_url
			});
			$('[data-show="valid-url"]').hide();
			$('[data-show="add-url"]').hide();
			$('[ng-message="patternValidator"]').hide();
		}, function() {
			// One of the sources is not available
			$('[data-show="valid-url"]').show();
		});
	}	
}
google.maps.event.addDomListener(window, getWebSiteLocation);

function resetLocationDetails() {
	$('#website-value').val('');
	updateLocationDetails({
		query: "0.0.0.0",
		country: "",
		regionName: "",
		city: "",
		timezone: "",
		lat: "",
		lon: ""
	});
	$("table").addClass("empty");
	initializePage();
}

function initializePage(){
	// handlebars templating
	window.indexTemplate = $('#index').html();
	window.locationTemplate = $('#locationInfo').html();

	window.indexTemplate = Handlebars.compile(window.indexTemplate);
	window.locationTemplate = Handlebars.compile(window.locationTemplate);

	$("#mainContent").html(window.indexTemplate());
	$("#geoLocationContainer").html(window.locationTemplate({
		id: 0,
		query: "0.0.0.0",
		country: "",
		regionName: "",
		city: "",
		timezone: "",
		lat: "",
		lon: ""
	}));

	var map;
	var webSitePosition = new google.maps.LatLng(37.7833, -122.4167);
	var mapOptions = {
		zoom: 10,
		center: webSitePosition,
		scrollwheel: false,
		disableDefaultUI: true,
	}

	var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

}

google.maps.event.addDomListener(window, 'load', initializePage);

$(document).ready(function(){
	initializePage();
});
